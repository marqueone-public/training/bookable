class AddRoleToUser < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :role, :bigint, default: 0
  end
end
